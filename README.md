# HttpsValve

This is a valve for Tomcat that redirects all http requests to https.

I'm sure the rewrite valve (https://tomcat.apache.org/tomcat-8.5-doc/rewrite.html) can do that,
but this class is much simpler.

I want to configure Tomcat to listen for http on 8080 for example and https on 8443,
but actually redirect all http requests to https.

I know I can add in conf/web.xml a security-constraint with a transport-guarantee CONFIDENTIAL,
but apparently that does not affect the manager app and the host-manager app.

## how to use

In a Context, add a Valve definition. For example, in conf/content.xml, like this:

    <Context>

        <!-- redirects http requests to https -->
        <Valve className="cat.inspiracio.catalina.HttpsValve" />

    </Context>

Add the https-valve-9.0.19.jar into lib/ of the Tomcat installation.

Compatible with Tomcat 9.0.19.