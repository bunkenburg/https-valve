/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.url;

import java.io.Serializable;

/** Initial implementation of URLUtils.
 * All methods throw NotImplementedException. 
 * 
 * Useful for extending it and making partial implementations. */
public class InitialURLUtils implements URLUtils, Serializable{
	private static final long serialVersionUID = -6971225855275869135L;

	protected InitialURLUtils(){}
	
	@Override public String getHref(){throw new UnsupportedOperationException();}
	@Override public void setHref(String href){throw new UnsupportedOperationException();}
	@Override public String getOrigin(){throw new UnsupportedOperationException();}
	@Override public String getProtocol(){throw new UnsupportedOperationException();}
	@Override public void setProtocol(String protocol){throw new UnsupportedOperationException();}
	@Override public String getUsername(){throw new UnsupportedOperationException();}
	@Override public void setUsername(String u){throw new UnsupportedOperationException();}
	@Override public String getPassword(){throw new UnsupportedOperationException();}
	@Override public void setPassword(String p){throw new UnsupportedOperationException();}
	@Override public String getHost(){throw new UnsupportedOperationException();}
	@Override public void setHost(String host){throw new UnsupportedOperationException();}
	@Override public String getHostname(){throw new UnsupportedOperationException();}
	@Override public void setHostname(String name){throw new UnsupportedOperationException();}
    @Override public int port(){throw new UnsupportedOperationException();}
    @Override public String getPort(){throw new UnsupportedOperationException();}
    @Override public void setPort(int port){throw new UnsupportedOperationException();}
    @Override public void setPort(String port){throw new UnsupportedOperationException();}
	@Override public String getPathname(){throw new UnsupportedOperationException();}
	@Override public void setPathname(String path){throw new UnsupportedOperationException();}
	@Override public String getSearch(){throw new UnsupportedOperationException();}
	@Override public void setSearch(String search){throw new UnsupportedOperationException();}
	@Override public URLSearchParams getSearchParams(){throw new UnsupportedOperationException();}
	@Override public void setSearchParams(URLSearchParams params){throw new UnsupportedOperationException();}
	@Override public String getHash(){throw new UnsupportedOperationException();}
	@Override public void setHash(String hash){throw new UnsupportedOperationException();}
}
