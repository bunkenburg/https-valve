/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.util;

import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

/** Like a map, but entries conserve their order and there may be several 
 * entries with the same key. 
 * 
 * A value cannot be null.
 * 
 * Keys and values have to serialisable, because I am interested in serialisable things. */
public class BagMap<K extends Serializable, V extends Serializable> implements Iterable<Entry<K,V>>, Serializable{
	private static final long serialVersionUID = 2587845281426723243L;

	// state ------------------------------------
	
	private List<Entry<K,V>> entries=new ArrayList<Entry<K,V>>();
	
	// construction ----------------------------
	
	/** Makes a new empty bag map. */
	public BagMap(){}
	
	// business --------------------------------
	
	/** Adds an entry. If the value is null, removes the parameter. 
	 * Returns this, for fluid style. */
	public BagMap<K,V> add(K key, V value){
		if(value==null){
			remove(key);
			return this;
		}
		Entry<K,V> entry=new EntryBean<K,V>(key, value);
		entries.add(entry);
		return this;
	}
	
	/** Gets the value of the first entry with that key, or null if there is none. */
	public V get(K key){
		for(Entry<K,V> entry : entries){
			if(equals(key, entry.getKey()))
				return entry.getValue();
		}
		return null;
	}
	
	/** Gets all the values for a key, in the order that they were inserted.
	 * Returns a fresh list. */
	public List<V> getValues(K key){
		List<V>values=new ArrayList<V>();
		for(Entry<K,V> entry : entries){
			K k=entry.getKey();
			if(equals(key, k))
				values.add(entry.getValue());
		}
		return values;
	}

	/** Is there an entry with this key? */
	public boolean containsKey(K key){
        for(Entry<K,V> entry : entries){
            K k=entry.getKey();
            if(equals(key, k))return true;
        }
        return false;
	}
	
	/** Removes all entries with this key. */
	public void remove(K key){
		Iterator<Entry<K,V>>it=entries.iterator();
		while(it.hasNext()){
			Entry<K,V> entry=it.next();
			if(equals(key, entry.getKey()))
				it.remove();
		}
	}
	
	/** Sets the first entry with this key to this value and removes all the others. 
	 * If there is no such entry, adds one. 
	 * 
	 * Setting a value to null is removing all. */
	public void set(K key, V value){
		if(value==null){
			remove(key);
			return;
		}
		boolean set=false;
		Iterator<Entry<K,V>>it=entries.iterator();
		while(it.hasNext()){
			Entry<K,V> entry=it.next();
			if(equals(key, entry.getKey())){
				if(set){
					it.remove();
				}else{
					entry.setValue(value);
					set=true;
				}
			}
		}
		if(!set)
			add(key, value);
	}
	
	public boolean isEmpty(){return entries.isEmpty();}
	public boolean empty(){return isEmpty();}

	/** All the entries in the order they were inserted. */
	@Override public Iterator<Entry<K,V>> iterator(){return entries.iterator();}
	
	/** All the entries in the order they were inserted. */
	public Iterable<Entry<K, V>> iteratable(){return entries;}

    /** All keys that have a value. */
    public Collection<K>keys(){
        Set<K>keys=new LinkedHashSet<K>();
        for(Entry<K,V> entry : entries){
            K key=entry.getKey();
            keys.add(key);
        }
        return keys;
    }

    /** All values. */
    public Collection<V>values(){
        Set<V>values=new LinkedHashSet<V>();
        for(Entry<K,V> entry : entries){
            V v=entry.getValue();
            values.add(v);
        }
        return values;
    }

    /** All entries. */
    public Collection<Entry<K, V>>entries(){return entries;}

	// helpers -----------------------------------------------

    /** Equality without stumbling over null. */
	private boolean equals(K a, K b){
		if(a==null)
			return b==null;
		return a.equals(b);
	}
	
	/** Implementation of interface Entry that is serialisable. */
	private static class EntryBean<K,V> implements Entry<K,V>, Serializable{
		private static final long serialVersionUID = -2527497268655890130L;

		private K key;
		private V value;

		EntryBean(K k, V v){key=k; value=v;}
		@Override public K getKey(){return key;}
		@Override public V getValue(){return value;}
		
		@Override public V setValue(V v){
			V old=value;
			value=v;
			return old;
		}
		
		/** Just for debug. */
		@Override public String toString(){
			return key + "->" + value;
		}
	}

}
