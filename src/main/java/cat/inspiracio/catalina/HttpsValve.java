/*
   Copyright 2019 Alexander Bunkenburg

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package cat.inspiracio.catalina;

import cat.inspiracio.url.URL;
import org.apache.catalina.Valve;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/** Redirects all http requests to https.
 * Sends 301 Permanent Redirect. Careful: browsers cache this. Not so easy to change your mind. */
public class HttpsValve extends ValveBase implements Valve {

    private Valve next;

    public HttpsValve(){}

    @Override public Valve getNext() { return next; }
    @Override public void setNext(Valve valve) { next = valve; }
    @Override public void backgroundProcess() { }

    /** Redirects all http requests to https. */
    @Override public void invoke(Request request, Response response) throws IOException, ServletException {
        if(request.isSecure()) {
            next.invoke(request, response);
        }
        else {

            Connector connector = request.getConnector();
            int port = connector.getPort();//8080
            int redirectPort = connector.getRedirectPort();//8443

            String url = url(request);
            String location = https(url, port, redirectPort);
            response.sendRedirect(location, 301);//Permanent Redirect
        }
    }

    @Override public boolean isAsyncSupported() { return false; }

    // helpers --------------------------------------------------------------

    private void say(Object o){
        System.out.println("" + o);
        System.err.println("" + o);
        containerLog.fatal("" + o);
    }

    /** Gets the full URL from a request. */
    private String url(HttpServletRequest request) {
        String requestURL = request.getRequestURL().toString();
        String queryString = request.getQueryString();
        if (queryString == null)
            return requestURL;
        else
            return requestURL + '?' + queryString;
    }

    /** Converts a http url to a https url. */
    private String https(String url, int port, int redirectPort){
        URL u = new URL(url);
        u.scheme("https");
        u.port(redirectPort);
        return u.toString();
    }
}
